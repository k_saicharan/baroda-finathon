import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { NgForm } from "@angular/forms";
import {AlertController, NavController} from "ionic-angular";
import 'rxjs/add/operator/map';


@Component({
    selector: 'page-user',
    templateUrl: 'lockeravali.html'
})


export class LockerAvaliPage {

    lockerAvaliData: any = {};

    submitted = false;

    constructor(public http: Http,public alertController: AlertController,public  navController: NavController) { }

    onLockerAvailGet(LockerForm: NgForm) {
        let json = JSON.stringify(
            {
                Cabinet_ID:LockerForm.controls['cabinetid'].value,
                Locker_Type:LockerForm.controls['lockerType'].value,
                Locker_Status:LockerForm.controls['lockerStatus'].value
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/LockAvailEnquiry",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.lockerAvaliData = data.json();
                        if(this.lockerAvaliData[0])
                        {
                            LockerForm.reset();
                            let alert = this.alertController.create({
                                title: "Locker Avaliability Information",
                                subTitle: '<b>Locker ID</b>'+' '+this.lockerAvaliData[0].Locker_ID+
                                '<br><b>Last Rent Date</b>'+' '+this.lockerAvaliData[0].Last_Rent_Date,
                                buttons: ['Dismiss']
                            });
                            alert.present();
                        }
                        else
                        {
                            alert("No Data Found");

                        }
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );
    }
}
