import {Component} from '@angular/core';

import 'rxjs/add/operator/map';
import {NavParams} from "ionic-angular";



@Component({
    selector: 'page-user',
    templateUrl: 'TDSPage.html',
    styleUrls: ['/pages/TDSPage/TDSPage.scss'],
})


export class TDSDetailedPage {

    TDSDetailedData : any;

    constructor(public navParams: NavParams) {
        this.TDSDetailedData = this.navParams.data;
    }

}