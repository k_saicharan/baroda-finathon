import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { NgForm } from "@angular/forms";
import {AlertController, NavController} from "ionic-angular";
import 'rxjs/add/operator/map';


@Component({
    selector: 'page-user',
    templateUrl: 'lockerdues.html'
})


export class LockerDuesPage {

    lockerDuesData: any = {};

    submitted = false;

    constructor(public http: Http,public alertController: AlertController,public  navController: NavController) { }

    onLockerDuesGet(LockerForm: NgForm) {
        let json = JSON.stringify(
            {
                Sol_ID:LockerForm.controls['serviceid'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/BrwiseLockerDues",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.lockerDuesData = data.json();
                        if(this.lockerDuesData)
                        {
                            LockerForm.reset();
                            let alert = this.alertController.create({
                                title: "Locker Information",
                                subTitle: '<b>Total Due</b>'+' '+this.lockerDuesData.Total_Maint_Due+
                                '<br><b>Charge Due</b>'+' '+this.lockerDuesData.Charge_Due,
                                buttons: ['Dismiss']
                            });
                            alert.present();
                        }
                        else
                        {
                            alert("No Data Found");

                        }
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );
    }
}
