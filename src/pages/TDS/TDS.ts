import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { NgForm } from "@angular/forms";
import {AlertController, NavController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {TDSDetailedPage} from "../TDSPage/TDSPage";


@Component({
    selector: 'page-user',
    templateUrl: 'TDS.html'
})


export class TDSPage {

    TDSData: any = {};

    submitted = false;

    constructor(public http: Http,public alertController: AlertController,public  navController: NavController) { }

    onTDSGet(TDSForm: NgForm) {
        let json = JSON.stringify(
            {
                Account_Number:TDSForm.controls['accountnumber'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/CustTDSEnquiry",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.TDSData = data.json();
                        if(this.TDSData)
                        {
                            TDSForm.reset();
                            this.navController.push(TDSDetailedPage,this.TDSData);
                        }
                        else
                        {
                            alert("No Data Found");

                        }
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );
    }
}
