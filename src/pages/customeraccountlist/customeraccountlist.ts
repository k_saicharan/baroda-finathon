import { Component } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {CustomerIDOptions} from "../../interfaces/customerid";


@Component({
selector: 'page-user',
templateUrl: 'customeraccountlist.html'
})

export class CustomerAccountPage {
    postDate:any;
customerid: CustomerIDOptions = {Customer_Id:0};
submitted = false;

constructor(public http: Http,) { }
  
onCustomerGet(customerid) {

    let json = JSON.stringify({Customer_Id:customerid});

    let headers = new Headers();
    headers.append('apikey', '691X2537T22044TG');
    headers.append("Content-Type",'application/json');
    let options = new RequestOptions({ headers: headers });
    
this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetCustAccList",json,options)
.subscribe (
    data => {
        if(data.status==200)
        {
            this.postDate = data.json();
        }
        else {
            alert("No Data Found");
        }
    },
        error =>
        {
            if(error.status==0)
            {
                alert("Request Failed");
            }
        }
);


}
}
