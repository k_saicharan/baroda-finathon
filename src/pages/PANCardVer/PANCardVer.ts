import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import { NgForm } from "@angular/forms";
import {AlertController} from "ionic-angular";
import 'rxjs/add/operator/map';



@Component({
    selector: 'page-user',
    templateUrl: 'PANCardVer.html'
})


export class PANPage {

    PANData: any = {};

    submitted = false;

    constructor(public http: Http,public alertController: AlertController) { }

    onPANGet(PANForm: NgForm) {
        let json = JSON.stringify(
            {
                Pan_Card:PANForm.controls['pancardnumber'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/PANCardRegVerification",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.PANData = data.json();
                        if(this.PANData[0])
                        {
                            PANForm.reset();
                            let alert = this.alertController.create({
                                title: "PAN CARD STATUS",
                                subTitle: '<h1><b>Customer Name: </b> '+this.PANData[0].First_Name+' '+this.PANData[0].Middle_Name+' '+this.PANData[0].Last_Name+'</h1><br>' +
                                '<h3><b>Customer ID:</b>'+this.PANData[0].Customer_Id + '</h3>',
                                buttons: ['Dismiss']
                            });
                            alert.present();
                        }
                        else
                        {
                            let alert = this.alertController.create({
                                title: "No Data Found",
                                buttons: ['Dismiss']
                            });
                            alert.present();
                        }
                    }
                    else {
                        let alert = this.alertController.create({
                            title: "Error",
                            subTitle: "Sorry Please contact Administrator",
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );
    }
}
