import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'cardauthorization.html'
})


export class CardAuthorizationPage {

    AuthorizationData:any;

    constructor(public http: Http,private datePipe:DatePipe,public alertController: AlertController) { }

    onCardAuthorizationGet(cardauthForm: NgForm) {
        let json = JSON.stringify(
            {
                Name:cardauthForm.controls['customerName'].value,
                CVV: cardauthForm.controls['cvv'].value,
                Expiry: this.datePipe.transform(cardauthForm.controls['expiry'].value,'yyyyMMdd') ,
                Amlount: cardauthForm.controls['amount'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/CardAuth",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {

                        this.AuthorizationData = data.json();
                        let alert = this.alertController.create({
                            title: "Authorization Status",
                            subTitle: this.AuthorizationData.Auth_Stat,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        cardauthForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
