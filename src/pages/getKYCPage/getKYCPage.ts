import {Component} from '@angular/core';

import 'rxjs/add/operator/map';
import {NavParams} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'getKYCPage.html'
})


export class KYCPage {

    KYCData : any;

    constructor(public navParams: NavParams) {
        this.KYCData = this.navParams.data;
    }

}