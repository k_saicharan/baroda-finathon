import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {AlertController, NavController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {LoanProductsDetailedPage} from "../loanproductspage/loanproductspage";



@Component({
    selector: 'page-user',
    templateUrl: 'loanproducts.html'
})

export class LoanProductsPage
{
    loanInformation : any;
    loading: boolean;

    constructor(public http: Http,public alertController: AlertController,public  navController: NavController) { }

    ionViewDidLoad() {
        this.loading = true;
        let json = JSON.stringify(
            {
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetTypLoanProd",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.loading = false;
                        this.loanInformation = data.json();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );
    }


    gotoLoanProductsDetailedPage(loanproductnumber:any)
    {
        this.navController.push(LoanProductsDetailedPage,loanproductnumber);
    }
}