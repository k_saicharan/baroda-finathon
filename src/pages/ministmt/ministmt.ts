import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";
import 'rxjs/add/operator/map';


@Component({
    selector: 'page-user',
    templateUrl: 'ministmt.html'
})


export class MiniStatementPage {

    miniStmtResponeData: any = {};

    submitted = false;

    constructor(public http: Http,public alertController: AlertController) { }

    onMiniStatementGet(transForm: NgForm) {
        let json = JSON.stringify(
            {
                Account_Number:transForm.controls['accountnumber'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetMiniState4anAccount",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                       console.log(data.json());
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );
    }
}
