import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";
import 'rxjs/add/operator/map';


@Component({
    selector: 'page-user',
    templateUrl: 'riskscore.html'
})

export class RiskScorePage {

    riskScoreData: any;

    constructor(public http: Http,public alertController: AlertController) { }

    onRiskScoreGet(riskScoreForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:riskScoreForm.controls['accountnumber'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetCustBehavRiskScore",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.riskScoreData = data.json();
                        if(this.riskScoreData[0])
                        {
                            let alert = this.alertController.create({
                                cssClass:'alert-success',
                                title: 'Your Risk Score is',
                                subTitle: this.riskScoreData[0].Behaviour_Risk_Score,
                                buttons: ['Dismiss'],
                            });
                            alert.present();
                            riskScoreForm.reset();
                        }
                        else
                        {
                            alert("No Data Found");
                        }
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }

}