import { Component } from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {AccountNumberOptions} from "../../interfaces/accountnumber";


@Component({
selector: 'page-user',
templateUrl: 'accountdetails.html'
})

export class AccountDetailsPage {
    accountData:any;
    accountnumber:AccountNumberOptions = {Account_Number:''}
    submitted = false;

constructor(public http: Http) { }
  
onAccountDetailsGet(accno) {

    let json = JSON.stringify({Account_Number:accno});

    let headers = new Headers();
    headers.append('apikey', '691X2537T22044TG');
    headers.append("Content-Type",'application/json');
    let options = new RequestOptions({ headers: headers });
    
this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetAccDetails",json,options)
.subscribe (
    data => {
        if(data.status==200)
        {
            this.accountData = data.json();
        }
        else {
            alert("No Data Found");
        }
    },
        error =>
        {
            if(error.status==0)
            {
                alert("Request Failed");
            }
        }
);


}
}
