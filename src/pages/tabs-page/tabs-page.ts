import { Component } from '@angular/core';

import { NavParams } from 'ionic-angular';


import { CustomerAccountPage } from '../customeraccountlist/customeraccountlist';
import { AccountDetailsPage } from "../accountdetails/accountdetails";
import { AccountTransactionsPage } from '../accounttransactions/acttrans';
import {MiniStatementPage} from "../ministmt/ministmt";
import {CustomerDetailPage} from "../customerdetail/customerdetail";
import {ModificationStatusPage} from "../modificationstatus/modificationstatus";



@Component({
  templateUrl: 'tabs-page.html'
})
export class TabsPage {
  // set the root pages for each tab
    tab1Root: any = CustomerAccountPage;
    tab2Root: any = AccountDetailsPage;
    tab3Root: any = AccountTransactionsPage;
    tab4Root: any = MiniStatementPage;
    tab5Root: any = CustomerDetailPage;
    tab6Root: any = ModificationStatusPage;


  mySelectedIndex: number;

  constructor(navParams: NavParams) {
    this.mySelectedIndex = navParams.data.tabIndex || 0;
  }

}
