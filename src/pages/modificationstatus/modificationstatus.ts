import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";
import 'rxjs/add/operator/map';


@Component({
    selector: 'page-user',
    templateUrl: 'modificationstatus.html'
})

export class ModificationStatusPage {

    modificationStatusData: any;

    constructor(public http: Http,public alertController: AlertController) { }

    onModificationStatusGet(modificationForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:modificationForm.controls['accountnumber'].value,
                SER_REF_NO:modificationForm.controls['referencenumber'].value
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/CustDetailsModStatusEnquiry",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.modificationStatusData = data.json();
                        if(this.modificationStatusData[0])
                        {
                            let alert = this.alertController.create({
                                cssClass:'alert-success',
                                title: this.modificationStatusData[0].Process_Status,
                                subTitle: this.modificationStatusData[0].Remark,
                                buttons: ['Dismiss'],
                            });
                            alert.present();
                            modificationForm.reset();
                        }
                        else
                        {
                            alert("No Data Found");
                        }
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }

}