import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'customertocustomer.html'
})


export class Customer2CustomerPage {
    c2cData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    oncust2custGet(c2cForm: NgForm) {
        let json = JSON.stringify(
            {
                Dr_Acct:c2cForm.controls['debit'].value,
                Cr_Acct :c2cForm.controls['credit'].value,
                Tran_Amt :c2cForm.controls['amount'].value,
                Tran_Msg :c2cForm.controls['message'].value,
            }
        );
        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/Cust2CustFundsTrf",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.c2cData = data.json();
                        let alert = this.alertController.create({
                            title: "Status: "+this.c2cData[0].Trans_Status,
                            subTitle: 'Reference Number:<br>'+this.c2cData[0].Trans_ref_No,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        c2cForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
