import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'retrieverewardpoints.html'
})


export class RetrieveRewardPointsPage {

    RetrieveRewardPointsData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onRetrieveRewardPointsGet(rewardsForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id :rewardsForm.controls['customerid'].value,
                Card_Number :rewardsForm.controls['cardnumber'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/RetrvRewardsPnt",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {

                        this.RetrieveRewardPointsData = data.json();
                        let alert = this.alertController.create({
                            title: "Status",
                            subTitle: 'Total Reward Points:<br>'+this.RetrieveRewardPointsData[0].Reward_point,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        rewardsForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
