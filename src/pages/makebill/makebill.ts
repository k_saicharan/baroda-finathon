import {Component} from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'makebill.html'
})


export class MakeBillerPage {

    makeBillerData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onmakeBillerGet(makeBillerForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:makeBillerForm.controls['customerid'].value,
                Debit_Acct:makeBillerForm.controls['debit'].value,
                BIller_ID:makeBillerForm.controls['biller'].value,
                Amount:makeBillerForm.controls['amount'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/MakeABillPayment",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.makeBillerData = data.json();
                        let alert = this.alertController.create({
                            title: "Reference Number",
                            subTitle: this.makeBillerData[0].Ser_Req_NUM,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        makeBillerForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
