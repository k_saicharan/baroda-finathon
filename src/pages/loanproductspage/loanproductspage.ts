import {Component} from '@angular/core';

import 'rxjs/add/operator/map';
import { Http, Headers, RequestOptions } from '@angular/http';
import {AlertController, NavController, NavParams} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'loanproductspage.html',
    styleUrls: ['/pages/loanproductspage/loanproductspage.scss'],
})


export class LoanProductsDetailedPage {

    loanProductDetailedData : any;

    constructor(public http: Http,public alertController: AlertController,public  navController: NavController,public navParams: NavParams) { }

    ionViewWillEnter() {
        let json = JSON.stringify(
            {
                product_code : this.navParams.data
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetLoanProdDetails",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.loanProductDetailedData = data.json();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );
    }


}