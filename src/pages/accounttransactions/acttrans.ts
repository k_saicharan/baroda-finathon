import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {AccountTransactionsOptions} from "../../interfaces/acctrans";
import {NgForm} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {AlertController} from "ionic-angular";


@Component({
selector: 'page-user',
templateUrl: 'acttrans.html'
})


export class AccountTransactionsPage {

    transactionData:any;

    accountTranaction:AccountTransactionsOptions = {
        Tran_Date: "",
        Value_Date: "",
        Transaction_Id: "",
        Transaction_Type: "",
        Tran_Amt: "",
        Tran_Rmks: "",
        Instrument_Number: "",
        Balance: ""
    };
    submitted = false;

constructor(public http: Http,private datePipe:DatePipe,public alertController: AlertController) { }
  
onTransactionsGet(transForm: NgForm) {
    let json = JSON.stringify(
            {
                Account_Number:transForm.controls['accountnumber'].value,
                Type_of_account: transForm.controls['accountType'].value,
                From_Date: this.datePipe.transform(transForm.controls['fromDate'].value,'yyyyMMdd') ,
                To_Date: this.datePipe.transform(transForm.controls['toDate'].value,'yyyyMMdd')
            }
        );

    let headers = new Headers();
    headers.append('apikey', '691X2537T22044TG');
    headers.append("Content-Type",'application/json');
    let options = new RequestOptions({ headers: headers });
    
this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetTransc4Account",json,options)
.subscribe (
    data => {
        if(data.status==200)
        {

                this.transactionData = data.json();
        }
        else {
            alert("Error");
        }
    },
        error =>
        {
            if(error.status==0)
            {
                let alert = this.alertController.create({
                    title: "Request Failed",
                    subTitle: 'No Internet Connection',
                    buttons: ['Dismiss']
                });
                alert.present();
            }
        }
);


}
}
