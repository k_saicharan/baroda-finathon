import {Component} from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'chequeissuance.html'
})


export class ChequeIssuancePage {

    makechequeData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onchequeGet(chqForm: NgForm) {
        let json = JSON.stringify(
            {
                Account_Number:chqForm.controls['accountnumber'].value,
                Cheque_Instrument_Type:chqForm.controls['chequetype'].value,
                Sub_Category: chqForm.controls['subchequetype'].value,
                Cheque_Per_Book: chqForm.controls['cheques'].value,
                Pick_Up_Branch: chqForm.controls['branch'].value,
                Number_of_Books: chqForm.controls['books'].value,
                SAN_Type: chqForm.controls['santype'].value,
                Cheque_Book_Amount: chqForm.controls['amount'].value,
                Is_P_Segment_Account: chqForm.controls['segement'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/CheqIssuance",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.makechequeData = data.json();
                        let alert = this.alertController.create({
                            title: "Status",
                            subTitle: 'Journal Number <br>'+this.makechequeData[0].Journal_Number+"<br>"+
                            'Status Code<br>'+this.makechequeData[0].Resp_Status,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        chqForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
