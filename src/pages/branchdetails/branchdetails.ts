import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'branchdetails.html'
})


export class BranchIFSCPage {
        branchIFSCData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onbranchIFSCGet(ifscForm: NgForm) {
        let json = JSON.stringify(
            {
                IFSC:ifscForm.controls['ifsc'].value,
            }
        );
        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetBrDetailsfrmIFSC",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.branchIFSCData = data.json();
                        ifscForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
