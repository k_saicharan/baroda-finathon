import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'userewardpoint.html'
})


export class UseRewardPointsPage {

    useRewardPointData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onuseRewardPointGet(userewardpointForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id :userewardpointForm.controls['customerid'].value,
                Card_Number :userewardpointForm.controls['cardnumber'].value,
                Biller_ID :userewardpointForm.controls['biller'].value,
                Amount :userewardpointForm.controls['amount'].value,
                Reward_point :userewardpointForm.controls['rewardpoint'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/UseRewPnt4BillPay",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {

                        this.useRewardPointData = data.json();
                        let alert = this.alertController.create({
                            title: "Status"+this.useRewardPointData[0].Status,
                            subTitle: '<b>Reference Number:</b><br>'+this.useRewardPointData[0].Trans_ref_No+"<br><br>"+
                            '<b>Balance Points</b><br>'+this.useRewardPointData[0].Reward_Points_Bal,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        userewardpointForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
