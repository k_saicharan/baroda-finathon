import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'addbiller.html'
})


export class AddBillerPage {
    addBillerData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onaddBillerGet(addBillerForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:addBillerForm.controls['customerid'].value,
                Biller_ID :addBillerForm.controls['billerid'].value
            }
        );
        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/AddBiller",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.addBillerData = data.json();
                        let alert = this.alertController.create({
                            title: "Request Number",
                            subTitle:this.addBillerData[0].Ser_Req_NUM,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        addBillerForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
