import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'stopcheque.html'
})


export class StopChequePage {
        stopData:any;

    constructor(public http: Http,private datePipe:DatePipe,public alertController: AlertController) { }

    onstopGet(scForm: NgForm) {
        let json = JSON.stringify(
            {
                Account_Number: scForm.controls['accountnumber'].value,
                From_Cheque_No: scForm.controls['fromcn'].value,
                To_Cheque_No: scForm.controls['tocn'].value,
                Stop_Issue_Date: this.datePipe.transform(scForm.controls['stopDate'].value,'yyyyMMdd'),
                Intrument_Type:scForm.controls['instrumentType'].value,
                Reason_Code: scForm.controls['resaon'].value,
                Stop_Comments: scForm.controls['comments'].value,
                Cheque_Type:scForm.controls['chequeType'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/StopCheque",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {

                        this.stopData = data.json();
                        let alert = this.alertController.create({
                            title: "Reference Number",
                            subTitle: this.stopData[0].Response_Number,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        scForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
