import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'addrewardpoints.html'
})


export class AddRewardPointsPage {

    rewardPointData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onrewardPointGet(addrewardpointForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id :addrewardpointForm.controls['customerid'].value,
                Card_Number :addrewardpointForm.controls['cardnumber'].value,
                Biller_ID :addrewardpointForm.controls['biller'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/AddRewardsPnt2Card",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {

                        this.rewardPointData = data.json();
                        let alert = this.alertController.create({
                            title: "Status",
                            subTitle: 'Request Number:<br>'+this.rewardPointData[0].Status,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        addrewardpointForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
