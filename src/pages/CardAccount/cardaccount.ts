import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController, NavController} from "ionic-angular";
import 'rxjs/add/operator/map';



@Component({
    selector: 'page-user',
    templateUrl: 'cardaccount.html'
})

export class CardAccountPage {

    cardAccountData: any;

    constructor(public http: Http,public alertController: AlertController,public navController: NavController) {
    }

    onCardSummaryGet(cardForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:cardForm.controls['customerid'].value,
                Card_Number:cardForm.controls['cardumber'].value,

            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/CardAcctSummary",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.cardAccountData = data.json();
                        if(this.cardAccountData[0])
                        {
                            cardForm.reset();
                        }
                        else
                        {
                            alert("No Data Found");
                        }
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }

}