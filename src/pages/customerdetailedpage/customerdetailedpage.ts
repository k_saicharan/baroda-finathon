import {Component} from '@angular/core';

import 'rxjs/add/operator/map';
import {NavParams} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'customerdetailedpage.html'
})


export class CustomerDetailedPage {

    customerDetailedData : any;

    constructor(public navParams: NavParams) {
        this.customerDetailedData = this.navParams.data;
    }

}