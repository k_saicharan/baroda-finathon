import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'changecreditlimit.html'
})


export class ChangeCreditLimitPage {

    changeCreditLimitData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onchangeCreditLimitGet(changelimitForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id :changelimitForm.controls['customerid'].value,
                Card_Number :changelimitForm.controls['cardnumber'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/ChangeCredLimit",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {

                        this.changeCreditLimitData = data.json();
                        let alert = this.alertController.create({
                            title: "Status",
                            subTitle: 'Request Number:<br>'+this.changeCreditLimitData[0].Request_Number,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        changelimitForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
