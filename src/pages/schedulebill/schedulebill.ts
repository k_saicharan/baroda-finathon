import {Component} from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";
import {DatePipe} from "@angular/common";


@Component({
    selector: 'page-user',
    templateUrl: 'schedulebill.html'
})


export class ScheduleBillerPage {

    scheduleBillerData:any;

    constructor(public http: Http,private datePipe:DatePipe,public alertController: AlertController) { }

    onscheduleBillerGet(scheduleBillerForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:scheduleBillerForm.controls['customerid'].value,
                Debit_Acct:scheduleBillerForm.controls['debit'].value,
                BIller_ID:scheduleBillerForm.controls['biller'].value,
                Amount:scheduleBillerForm.controls['amount'].value,
                Date:this.datePipe.transform(scheduleBillerForm.controls['toDate'].value,'yyyyMMdd')
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/SchedaBillPay",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.scheduleBillerData = data.json();
                        let alert = this.alertController.create({
                            title: "Reference Number",
                            subTitle: this.scheduleBillerData[0].Ser_Req_NUM,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        scheduleBillerForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
