import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController} from "ionic-angular";


@Component({
    selector: 'page-user',
    templateUrl: 'issuecreditlimit.html'
})


export class IssueCreditLimitPage {

    issueCreditLimitData:any;

    constructor(public http: Http,public alertController: AlertController) { }

    onissueCreditLimitGet(cardauthForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:cardauthForm.controls['customerid'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/IssueCredLimit",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {

                        this.issueCreditLimitData = data.json();
                        let alert = this.alertController.create({
                            title: "Status",
                            subTitle: 'Request Number'+this.issueCreditLimitData[0].Request_Number,
                            buttons: ['Dismiss']
                        });
                        alert.present();
                        cardauthForm.reset();
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
