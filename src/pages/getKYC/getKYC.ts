import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController, NavController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {KYCPage} from "../getKYCPage/getKYCPage";


@Component({
    selector: 'page-user',
    templateUrl: 'getKYC.html'
})

export class GetKYCPage {

    KYCData: any;

    constructor(public http: Http,public alertController: AlertController,public navController: NavController) {
    }

    onKYCGet(KYCForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:KYCForm.controls['accountnumber'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetKYCDetails",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.KYCData = data.json();
                        if(this.KYCData[0])
                        {
                            this.navController.push(KYCPage,this.KYCData)
                            KYCForm.reset();
                        }
                        else
                        {
                            alert("No Data Found");
                        }
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }

}