import {Component} from '@angular/core';

import { Http, Headers, RequestOptions } from '@angular/http';
import {NgForm} from "@angular/forms";
import {AlertController, NavController} from "ionic-angular";
import 'rxjs/add/operator/map';
import {CustomerDetailedPage} from "../customerdetailedpage/customerdetailedpage";


@Component({
    selector: 'page-user',
    templateUrl: 'customerdetail.html'
})


export class CustomerDetailPage {

    customerData: any = {};
    submitted = false;
    constructor(public http: Http,public alertController: AlertController,public navController:NavController) { }

    onCustomerDetailPage(CustomerForm: NgForm) {
        let json = JSON.stringify(
            {
                Customer_Id:CustomerForm.controls['customerid'].value,
            }
        );

        let headers = new Headers();
        headers.append('apikey', '691X2537T22044TG');
        headers.append("Content-Type",'application/json');
        let options = new RequestOptions({ headers: headers });

        this.http.post("http://104.211.176.248:8080/bob/bobuat/api/GetCustDetails",json,options)
            .subscribe (
                data => {
                    if(data.status==200)
                    {
                        this.customerData = data.json();
                        if(this.customerData[0])
                        {
                            CustomerForm.reset();
                            this.navController.push(CustomerDetailedPage,this.customerData);
                        }
                        else
                        {
                            alert("No Data Found");

                        }
                    }
                    else {
                        alert("Error");
                    }
                },
                error =>
                {
                    if(error.status==0)
                    {
                        let alert = this.alertController.create({
                            title: "Request Failed",
                            subTitle: 'No Internet Connection',
                            buttons: ['Dismiss']
                        });
                        alert.present();
                    }
                }
            );


    }
}
