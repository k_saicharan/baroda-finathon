export interface AccountTransactionsOptions
{
    Tran_Date: any,
    Value_Date: any,
    Transaction_Id: any,
    Transaction_Type: string,
    Tran_Amt: any,
    Tran_Rmks: string,
    Instrument_Number: any,
    Balance: any

}