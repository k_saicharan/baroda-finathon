import { BrowserModule } from '@angular/platform-browser';
import { HttpModule } from '@angular/http';
import { NgModule, ErrorHandler } from '@angular/core';

import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';

import { InAppBrowser } from '@ionic-native/in-app-browser';
import { SplashScreen } from '@ionic-native/splash-screen';

import { IonicStorageModule } from '@ionic/storage';

import { ConferenceApp } from './app.component';

import { MapPage } from '../pages/map/map';
import { CustomerAccountPage } from '../pages/customeraccountlist/customeraccountlist';
import { TabsPage } from '../pages/tabs-page/tabs-page';
import {MiniStatementPage} from "../pages/ministmt/ministmt";
import {AccountDetailsPage} from "../pages/accountdetails/accountdetails";
import {AccountTransactionsPage} from "../pages/accounttransactions/acttrans";

import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';

import {FormsModule} from "@angular/forms";
import {DatePipe} from "@angular/common";
import {CustomerDetailPage} from "../pages/customerdetail/customerdetail";
import {CustomerDetailedPage} from "../pages/customerdetailedpage/customerdetailedpage";
import {ModificationStatusPage} from "../pages/modificationstatus/modificationstatus";
import {RiskScorePage} from "../pages/riskscore/riskscore";
import {GetKYCPage} from "../pages/getKYC/getKYC";
import {KYCPage} from "../pages/getKYCPage/getKYCPage";
import {TDSPage} from "../pages/TDS/TDS";
import {TDSDetailedPage} from "../pages/TDSPage/TDSPage";
import {PANPage} from "../pages/PANCardVer/PANCardVer";
import {rootPage} from "../pages/rootpage/rootpage";
import {LoanProductsPage} from "../pages/loanproducts/loanproducts";
import {LoanProductsDetailedPage} from "../pages/loanproductspage/loanproductspage";
import {LockerDuesPage} from "../pages/lockerdues/lockerdues";
import {LockerAvaliPage} from "../pages/lockeravali/lockeravali";
import {CardAccountPage} from "../pages/CardAccount/cardaccount";
import {CardAuthorizationPage} from "../pages/CardAuthorization/cardauthorization";
import {IssueCreditLimitPage} from "../pages/issuecreditlimit/issuecreditlimit";
import {ChangeCreditLimitPage} from "../pages/changecreditlimit/changecreditlimit";
import {OverseasUsePage} from "../pages/overseasuse/overseasuse";
import {BlockCardPage} from "../pages/blockcard/blockcard";
import {RetrieveRewardPointsPage} from "../pages/retrieverewardpoints/retrieverewardpoints";
import {AddRewardPointsPage} from "../pages/addrewardpoints/addrewardpoints";
import {UseRewardPointsPage} from "../pages/userewardpoint/userewardpoint";
import {BranchIFSCPage} from "../pages/branchdetails/branchdetails";
import {Customer2CustomerPage} from "../pages/customertocustomer/customertocustomer";
import {AddBillerPage} from "../pages/addbiller/addbiller";
import {GetBillerPage} from "../pages/getbiller/getbiller";
import {MakeBillerPage} from "../pages/makebill/makebill";
import {ScheduleBillerPage} from "../pages/schedulebill/schedulebill";
import {ChequeIssuancePage} from "../pages/chequeissuance/chequeissuance";
import {StopChequePage} from "../pages/stopcheque/stopcheque";


@NgModule({
  declarations: [
      ConferenceApp,
      MapPage,
      TabsPage,
      rootPage,
      CustomerAccountPage,
      AccountDetailsPage,
      AccountTransactionsPage,
      MiniStatementPage,
      CustomerDetailPage,
      CustomerDetailedPage,
      ModificationStatusPage,
      RiskScorePage,
      GetKYCPage,
      KYCPage,
      TDSPage,
      TDSDetailedPage,
      PANPage,
      LoanProductsPage,
      LoanProductsDetailedPage,
      LockerDuesPage,
      LockerAvaliPage,
      CardAccountPage,
      CardAuthorizationPage,
      IssueCreditLimitPage,
      ChangeCreditLimitPage,
      OverseasUsePage,
      BlockCardPage,
      RetrieveRewardPointsPage,
      AddRewardPointsPage,
      UseRewardPointsPage,
      BranchIFSCPage,
      Customer2CustomerPage,
      AddBillerPage,
      GetBillerPage,
      MakeBillerPage,
      ScheduleBillerPage,
      ChequeIssuancePage,
      StopChequePage
  ],
  imports: [
    BrowserModule,
    HttpModule,
      FormsModule,
    IonicModule.forRoot(ConferenceApp, {}, {
      links: [
          { component: TabsPage, name: 'TabsPage', segment: 'tabs-page' },
          { component: CustomerAccountPage, name: 'CustomerAccountPage', segment: 'customeraccountlist' },
          { component: AccountDetailsPage, name: 'AccountDetailsPage', segment: 'accountdetails' },
          { component: AccountTransactionsPage, name: 'AccountTransactionsPage', segment: 'acttrans' },
          { component: MiniStatementPage, name: 'MiniStatementPage', segment: 'ministmt' },
          { component: CustomerDetailPage, name: 'CustomerDetailPage', segment: 'customerdetail' },
          { component: MapPage, name: 'MapPage', segment: 'map' },
          { component: ModificationStatusPage, name: 'ModificationStatusPage', segment: 'modificationstatus' },
          { component: RiskScorePage, name: 'RiskScorePage', segment: 'riskscore' },
          { component: GetKYCPage, name: 'GetKYCPage', segment: 'getKYC' },
          { component: TDSPage, name: 'TDSPage', segment: 'TDS' },
          { component: PANPage, name: 'PANPage', segment: 'PANCardVer' },
          { component: LoanProductsPage, name: 'LoanProductsPage', segment: 'loanproducts' },
          { component: LockerDuesPage, name: 'LockerDuesPage', segment: 'lockerdues' },
          { component: LockerAvaliPage, name: 'LockerAvaliPage', segment: 'lockeravali' },
          { component: CardAccountPage, name: 'CardAccountPage', segment: 'cardaccount' },
          { component: CardAuthorizationPage, name: 'CardAuthorizationPage', segment: 'cardauthorizationPage' },
          { component: IssueCreditLimitPage, name: 'IssueCreditLimitPage', segment: 'issuecreditlimit' },
          { component: ChangeCreditLimitPage, name: 'ChangeCreditLimitPage', segment: 'changecreditlimit' },
          { component: OverseasUsePage, name: 'OverseasUsePage', segment: 'overseasuse' },
          { component: BlockCardPage, name: 'BlockCardPage', segment: 'blockcard' },
          { component: RetrieveRewardPointsPage, name: 'RetrieveRewardPointsPage', segment: 'retrieverewardpoints' },
          { component: AddRewardPointsPage, name: 'AddRewardPointsPage', segment: 'addrewardpoints' },
          { component: UseRewardPointsPage, name: 'UseRewardPointsPage', segment: 'userewardpoint' },
          { component: BranchIFSCPage, name: 'BranchIFSCPage', segment: 'branchdetails' },
          { component: Customer2CustomerPage, name: 'Customer2CustomerPage', segment: 'customertocustomer' },
          { component: AddBillerPage, name: 'AddBillerPage', segment: 'addbiller' },
          { component: GetBillerPage, name: 'GetBillerPage', segment: 'getbiller' },
          { component: MakeBillerPage, name: 'MakeBillerPage', segment: 'makebill' },
          { component: ScheduleBillerPage, name: 'ScheduleBillerPage', segment: 'schedulebill' },
          { component: ChequeIssuancePage, name: 'ChequeIssuancePage', segment: 'chequeissuance' },
          { component: StopChequePage, name: 'StopChequePage', segment: 'stopcheque' }
      ]
    }),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
      ConferenceApp,
      MapPage,
      TabsPage,
      rootPage,
      CustomerAccountPage,
      AccountDetailsPage,
      AccountTransactionsPage,
      MiniStatementPage,
      CustomerDetailPage,
      CustomerDetailedPage,
      ModificationStatusPage,
      RiskScorePage,
      GetKYCPage,
      KYCPage,
      TDSPage,
      TDSDetailedPage,
      PANPage,
      LoanProductsPage,
      LoanProductsDetailedPage,
      LockerDuesPage,
      LockerAvaliPage,
      CardAccountPage,
      CardAuthorizationPage,
      IssueCreditLimitPage,
      ChangeCreditLimitPage,
      OverseasUsePage,
      BlockCardPage,
      RetrieveRewardPointsPage,
      AddRewardPointsPage,
      UseRewardPointsPage,
      BranchIFSCPage,
      Customer2CustomerPage,
      AddBillerPage,
      GetBillerPage,
      MakeBillerPage,
      ScheduleBillerPage,
      ChequeIssuancePage,
      StopChequePage
  ],
  providers: [
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    ConferenceData,
    UserData,
    InAppBrowser,
        SplashScreen,
      DatePipe
  ]
})
export class AppModule {
    root
}
