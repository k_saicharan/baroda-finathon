import { Component, ViewChild } from '@angular/core';
import { Events, MenuController, Nav, Platform } from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { Storage } from '@ionic/storage';

import { ConferenceData } from '../providers/conference-data';
import { UserData } from '../providers/user-data';
import { AccountDetailsPage } from "../pages/accountdetails/accountdetails";
import { AccountTransactionsPage } from '../pages/accounttransactions/acttrans';
import { CustomerAccountPage } from '../pages/customeraccountlist/customeraccountlist';
import { MiniStatementPage } from "../pages/ministmt/ministmt";
import { CustomerDetailPage } from "../pages/customerdetail/customerdetail";
import { ModificationStatusPage } from "../pages/modificationstatus/modificationstatus";
import {RiskScorePage} from "../pages/riskscore/riskscore";
import {GetKYCPage} from "../pages/getKYC/getKYC";
import {TDSPage} from "../pages/TDS/TDS";
import {PANPage} from "../pages/PANCardVer/PANCardVer";
import {LoanProductsPage} from "../pages/loanproducts/loanproducts";
import {LockerDuesPage} from "../pages/lockerdues/lockerdues";
import {LockerAvaliPage} from "../pages/lockeravali/lockeravali";
import {CardAccountPage} from "../pages/CardAccount/cardaccount";
import {CardAuthorizationPage} from "../pages/CardAuthorization/cardauthorization";
import {IssueCreditLimitPage} from "../pages/issuecreditlimit/issuecreditlimit";
import {ChangeCreditLimitPage} from "../pages/changecreditlimit/changecreditlimit";
import {OverseasUsePage} from "../pages/overseasuse/overseasuse";
import {BlockCardPage} from "../pages/blockcard/blockcard";
import {RetrieveRewardPointsPage} from "../pages/retrieverewardpoints/retrieverewardpoints";
import {AddRewardPointsPage} from "../pages/addrewardpoints/addrewardpoints";
import {UseRewardPointsPage} from "../pages/userewardpoint/userewardpoint";
import {rootPage} from "../pages/rootpage/rootpage";
import {BranchIFSCPage} from "../pages/branchdetails/branchdetails";
import {Customer2CustomerPage} from "../pages/customertocustomer/customertocustomer";
import {AddBillerPage} from "../pages/addbiller/addbiller";
import {GetBillerPage} from "../pages/getbiller/getbiller";
import {MakeBillerPage} from "../pages/makebill/makebill";
import {ScheduleBillerPage} from "../pages/schedulebill/schedulebill";
import {ChequeIssuancePage} from "../pages/chequeissuance/chequeissuance";
import {StopChequePage} from "../pages/stopcheque/stopcheque";


export interface PageInterface {
title: string;
name: string;
component: any;
icon?: string;
logsOut?: boolean;
index?: number;
tabName?: string;
tabComponent?: any;
}

@Component({
templateUrl: 'app.template.html'
})
export class ConferenceApp {
// the root nav is a child of the root app component
// @ViewChild(Nav) gets a reference to the app's root nav
@ViewChild(Nav) nav: Nav;

// List of pages that can be navigated to from the left menu
// the left menu only works after login
// the login page disables the left menu
customerPages: PageInterface[] = [
    { title: 'Customer Account List', name: 'CustomerAccountPage', component: CustomerAccountPage },
    { title: 'Account Details', name: 'AccountDetailsPage', component: AccountDetailsPage},
    { title: 'Transaction of Account', name: 'AccountTransactionsPage', component: AccountTransactionsPage },
    { title: 'Mini Statement of Account', name: 'MiniStatementPage', component: MiniStatementPage},
    { title: 'Customer Details', name: 'CustomerDetailPage', component: CustomerDetailPage },
    { title: 'Modify Status', name: 'ModificationStatusPage', component: ModificationStatusPage },
    { title: 'Risk Score', name: 'RiskScorePage', component: RiskScorePage },
    { title: 'Get KYC', name: 'GetKYCPage', component: GetKYCPage },
    { title: 'TDS Enquiry', name: 'TDSPage', component: TDSPage},
    { title: 'PAN Verification', name: 'PANPage', component: PANPage }
];

paymentPages: PageInterface[] = [
{ title: 'Branch Details with IFSC', name: 'BranchIFSCPage', component: BranchIFSCPage},
/*{ title: 'NEFT Transfer', name: 'SupportPage', component: LoanProductsPage, icon: 'help' },
{ title: 'RTGS Transfer', name: 'SignupPage', component: LoanProductsPage, icon: 'person-add' },*/
{ title: 'C 2 C Transfer', name: 'Customer2CustomerPage', component: Customer2CustomerPage },
{ title: 'Add Billers', name: 'AddBillerPage', component: AddBillerPage },
{ title: 'Get Billers', name: 'GetBillerPage', component: GetBillerPage },
{ title: 'Make Bill Payment', name: 'MakeBillerPage', component: MakeBillerPage },
{ title: 'Schedule a Bill Payment', name: 'ScheduleBillerPage', component: ScheduleBillerPage }
];

loanPages: PageInterface[] = [
{ title: 'Loan Products', name: 'LoanProductsPage', component: LoanProductsPage }
];

lockerPages: PageInterface[] = [
    { title: 'Locker Dues', name: 'LockerDuesPage', component: LockerDuesPage },
    { title: 'Locker Avaliability Status', name: 'LockerAvaliPage', component: LockerAvaliPage }
];

chequePages: PageInterface[] = [
{ title: 'Cheque Issuance', name: 'ChequeIssuancePage', component: ChequeIssuancePage },
{ title: 'Stop Cheque', name: 'StopChequePage', component: StopChequePage }
];

/*atmPages: PageInterface[] = [
{ title: 'Radius Search', name: 'LoginPage', component: LoanProductsPage, icon: 'log-in' },
{ title: 'City Wise Search', name: 'SupportPage', component: LoanProductsPage, icon: 'help' }
];*/

cardPages: PageInterface[] = [
{ title: 'Card Account Summary', name: 'CardAccountPage', component: CardAccountPage },
{ title: 'Card Authorization', name: 'CardAuthorizationPage', component: CardAuthorizationPage },
{ title: 'Issue Credit Limit', name: 'IssueCreditLimitPage', component: IssueCreditLimitPage },
{ title: 'Change Credit Limit', name: 'ChangeCreditLimitPage', component: ChangeCreditLimitPage },
{ title: 'Activate Card for Overseas', name: 'OverseasUsePage', component: OverseasUsePage },
{ title: 'Block Card', name: 'BlockCardPage', component: BlockCardPage },
{ title: 'Add Reward points to card', name: 'AddRewardPointsPage', component: AddRewardPointsPage },
{ title: 'Retrieve Reward points to card', name: 'RetrieveRewardPointsPage', component: RetrieveRewardPointsPage },
{ title: 'Use of Reward points', name: 'UseRewardPointsPage', component: UseRewardPointsPage }
];

rootpage : any = rootPage;

constructor(public events: Events,public userData: UserData,public menu: MenuController, public platform: Platform, public confData: ConferenceData, public storage: Storage, public splashScreen: SplashScreen
) {

// load the conference data
confData.load();

// decide which menu items should be hidden by current login status stored in local storage
this.enableMenu(true);
}

openPage(page: PageInterface) {

let params = {};

// the nav component was found using @ViewChild(Nav)
// setRoot on the nav to remove previous pages and only have this page
// we wouldn't want the back button to show in this scenario
if (page.index) {
params = { tabIndex: page.index };
}

// If we are already on tabs just change the selected tab
// don't setRoot again, this maintains the history stack of the
// tabs even if changing them from the menu
if (this.nav.getActiveChildNavs().length && page.index != undefined) {
this.nav.getActiveChildNavs()[0].select(page.index);
} else {
// Set the root of the nav with params if it's a tab index
this.nav.setRoot(page.name, params).catch((err: any) => {
console.log(`Didn't set nav root: ${err}`);
});
}

}


enableMenu(loggedIn: boolean) {
this.menu.enable(loggedIn, 'mainMenu');
}

platformReady() {
// Call any initial plugins when ready
this.platform.ready().then(() => {
this.splashScreen.hide();
});
}

isActive(page: PageInterface) {
let childNav = this.nav.getActiveChildNavs()[0];

// Tabs are a special case because they have their own navigation
if (childNav) {
if (childNav.getSelected() && childNav.getSelected().root === page.tabComponent) {
return 'primary';
}
return;
}

if (this.nav.getActive() && this.nav.getActive().name === page.name) {
return 'primary';
}
return;
}

}
